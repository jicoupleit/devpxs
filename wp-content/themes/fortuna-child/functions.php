<?php
/*This file is part of child-theme, fortuna child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function pxs_child_theme_enqueue_child_styles() {
$parent_style = 'parent-style'; 
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 
		'child-style', 
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version') );
	}
add_action( 'wp_enqueue_scripts', 'pxs_child_theme_enqueue_child_styles' );

/*Write here your own functions */

// Register Custom Post Type
function pxs_event_post_type() {

	$labels = array(
		'name'                  => _x( 'Event', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Event', 'text_domain' ),
		'name_admin_bar'        => __( 'Event', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'text_domain' ),
		'description'           => __( 'Event information page.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'event', $args );

}
//add_action( 'init', 'pxs_event_post_type', 0 );

/**	
	Page Header method
**/
if( ! function_exists( 'pxs_boc_page_header' ) ) { 
	function pxs_boc_page_header() {

		// Get post id if on a post or page
		if(is_archive() || is_search() || is_tax()){
			$post_id = 0;
		}elseif(is_home()) {
			$post_id = get_option( 'page_for_posts' );
		}else {
			global $post;
			if(isset($post)){
				$post_id = $post->ID;
			}else {
				$post_id = 0;
			}
		}
		
		global $woocommerce;
		
		// If shop get shop page id
		if(function_exists('is_shop') && is_shop()){

			if(function_exists('wc_get_page_id')){
				$post_id = wc_get_page_id('shop');
			}else {
				$post_id = woocommerce_get_page_id('shop');
			}
		}
		
		// Shall we display the page heading according to Post meta
		$boc_show_heading = get_post_meta($post_id, 'boc_page_heading_set', true);
		$boc_content_top_margin = (get_post_meta($post_id, 'boc_content_top_margin', true)!=='off'? true : false);


		if($boc_show_heading!=='off') {
		
			if(is_archive() || is_search() || is_home() || is_404() || is_page() || is_tax() || is_single()) { 
			
				$boc_page_breadcrumbs = (get_post_meta($post_id, 'boc_page_breadcrumbs', true)!=='off'? true : false);
				$extra_style = "";
				if($boc_page_breadcrumbs){
					$breadcrumbs_position =  ot_get_option('breadcrumbs_position','floated');
					if($breadcrumbs_position!="normal"){
						$extra_style = " style='padding: 20px 0;'";
					}
				}
			?>
				<div class="full_container_page_title <?php echo ($boc_content_top_margin ? '' : 'no_bm');?>" <?php echo $extra_style;?>>	
					<div class="container">		
						<div class="section no_bm">
								<?php 
								if($boc_page_breadcrumbs) {
									boc_breadcrumbs(); 
								}
								?>
								
								<div class="page_heading test"><h1>
								<?php 	if (is_home() && is_front_page()) {
											esc_html(bloginfo('name'));
											
										}elseif(is_home()){
											esc_html(single_post_title(''));
											
										}elseif(is_404()){
											_e('404 - Page Not Found', 'Fortuna');
											
										}elseif(is_archive() && !(function_exists('is_woocommerce') && is_woocommerce())){
											if(is_year()){ echo esc_html(get_the_time('Y')); }
											elseif(is_month()){ echo esc_html(get_the_time('F Y')); }
											elseif(is_day()){ echo esc_html(get_the_time('F jS, Y')); }
											elseif(is_author()) { 	echo esc_html(get_the_author()); }
											elseif(is_tag()) { 	echo __("Tag", 'Fortuna').": ". esc_html(single_tag_title('',false)); }
											else{ 
												echo esc_html(single_cat_title());
											}
										
										}elseif(is_search()){
											echo __('Search results for:', 'Fortuna').' '. esc_html(get_search_query());									
									
										}elseif(is_page()){
											esc_html(the_title(''));
															
										}elseif(is_singular()){
											echo "News";
															
										}elseif(is_singular('event')){
											echo "Events";

										}elseif(is_tax()){
											echo esc_html(single_cat_title());

											
										}elseif((function_exists('is_woocommerce') && is_woocommerce()) && (is_shop() || is_product_category() || is_product_tag())) {
											echo (is_archive() ? _e('Shop', 'Fortuna') : 
														(is_search() ? _e('Search results for:', 'Fortuna').' '. esc_html(get_search_query()): 
																(is_home() ? esc_html(the_title('')) : esc_html(the_title())) ));
										}else {
											esc_html(the_title(''));

										}
								?>
								</h1></div>	
						</div>
					</div>
				</div>
			<?php			
			}else {
				if($boc_content_top_margin){
						echo '<div class="h20"></div>';
				}
			} 
	
		}else {
			if(get_page_template_slug( $post_id ) == "portfolio-three-column.php") {
				echo '<div class="h100"></div>';				
			}
			if($boc_content_top_margin){
				// For any page other than the contact Page template
				if(get_page_template_slug( $post_id ) != "contact.php") {
					echo '<div class="h20"></div>';
				}
			}	
		}
	}
}
add_shortcode('post_id', 'post_shortcode_query');
function post_shortcode_query($atts, $content){
	extract(shortcode_atts(array( // a few default values
			'posts_per_page' => '1',
			'post_type' => 'post',
			'caller_get_posts' => 1)
		, $atts));

	global $post;

	$posts = new WP_Query($atts);
	$output = '';
	if ($posts->have_posts())
		while ($posts->have_posts()):
			$posts->the_post();
			$out = '<div class="post_item_block boc_bottom-to-top">
				<div class="pic">
					<a href="'.get_permalink().'" title="' . get_the_title() . '">'.get_the_post_thumbnail() .'<div class="img_overlay"><span class="hover_icon icon_plus"></span></div></a>
				</div>
				<div class="post_item_desc dark_links">
                <h4><a href="'.get_permalink().'" title="' . get_the_title() . '">'.get_the_title() .'</a></h4>
                <p class="the_desc">'.wp_trim_words( get_the_content(), 10, '...' ).'</p>
				<a href="'.get_permalink().'" class="more-link2">Read mores</a>
				</div>';

			$out .='</div>';
		endwhile;
	else
		return; // no posts found

	wp_reset_query();
	return html_entity_decode($out);
}