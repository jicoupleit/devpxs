<?php
/**
 * Created by PhpStorm.
 * User: macintosh
 * Date: 8/1/17
 * Time: 10:32 PM
 * Plugin Name: Show Post ID
Plugin URI: http://www.braylynhomes.com/
Description: Show Bunch Portfolio Shortcode
Author: Ronalduc
Author URI: https://jidevexperts.com/
Text Domain: https://jidevexperts.com/
 */

class Show_Post_ID {

    function __construct() {

        add_shortcode('show_post_id' ,array($this,'show_post_id'));

    }
    function show_post_id($atts, $content){ ?>
        <div class="lvca-portfolio-wrap show-portfolio-header">
            <div class="row list_portfolio" id="lightgallery">
                <?php
                extract(shortcode_atts(array( // a few default values
                        'posts_per_page' => '1',
                        'post_type' => 'post',
                        'caller_get_posts' => 1)
                    , $atts));

                global $post;
                $the_query = new WP_Query( $atts );
                if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post()  ;

                        ?>
                        <div class="post_item_block boc_anim_hidden boc_bottom-to-top  boc_start_animation" <?php if (! has_post_thumbnail() ) { echo ' class="no-img"'; } ?>>
                            <div class="pic">
                                <a href="<?php the_permalink(); ?>">
                                    <?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('alm-thumbnail');
                                    }
                                    ?>
                                    <div class="img_overlay"><span class="hover_icon icon_plus"></span></div>
                                </a>
                            </div>
                            <div class="post_item_desc dark_links">
                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="more-link2">Read mores</a>
                            </div>
                        </div>
                    <?php endwhile;  wp_reset_postdata();  else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            </div>

        </div>

    <?php }
}
new Show_Post_ID();
